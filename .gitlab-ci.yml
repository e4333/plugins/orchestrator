# You can copy and paste this template into a new `.gitlab-ci.yml` file.
# You should not add this template to an existing `.gitlab-ci.yml` file by using the `include:` keyword.
# To contribute improvements to CI/CD templates, please follow the Development guide at:
# https://docs.gitlab.com/ee/development/cicd/templates.html
# This specific template is located at:
# https://gitlab.com/gitlab-org/gitlab/-/blob/master/lib/gitlab/ci/templates/Gradle.gitlab-ci.yml

# This is the Gradle build system for JVM applications
# https://gradle.org/
# https://github.com/gradle/gradle

image: gradle:alpine

before_script:
  - GRADLE_USER_HOME="$(pwd)/.gradle"
  - export GRADLE_USER_HOME

variables:
  PACKAGE_REGISTRY_URL: ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic
  PLUGIN: ${CI_PROJECT_NAME}

stages:
  - test
  - build
  - release

sast:
  stage: test

include:
- template: Security/SAST.gitlab-ci.yml

test:
  stage: test
  script: gradle test --warning-mode all
  coverage: '/    - Instruction Coverage: ([0-9.]+)%/'
  cache:
    key: "$CI_COMMIT_REF_NAME"
    policy: pull
    paths:
      - build
      - .gradle

# checkstyle:
#   stage: test
#   script:
#     - gradle checkstyleMain
#     - gradle checkstyleTest
#   allow_failure: true

build:
  stage: build
  needs:
    - test
  variables:
    FILE: build/libs/${PLUGIN}.jar
  script:
    - apk update
    - apk add curl
    - gradle --build-cache assemble
    - |
      curl --header "JOB-TOKEN: ${CI_JOB_TOKEN}" --upload-file "${FILE}" "${PACKAGE_REGISTRY_URL}/${CI_COMMIT_TAG}/plugin/${PLUGIN}.jar"
  cache:
    key: "$CI_COMMIT_REF_NAME"
    policy: push
    paths:
      - build
      - .gradle

  rules:
    - if: '$CI_COMMIT_TAG && $CI_COMMIT_TAG =~ /^v[0-9]\.[0-9]\.[0-9](-rc.+)?/'
      when: always

create_release:
  stage: release
  needs:
    - job: build
      artifacts: true
  image: registry.gitlab.com/gitlab-org/release-cli:latest
  variables:
    ASSET: "{\"name\":\"${PLUGIN}-${CI_COMMIT_TAG}\",\"url\":\"${PACKAGE_REGISTRY_URL}/${CI_COMMIT_TAG}/plugin/${PLUGIN}.jar\",\"type\":\"package\"}"
  script:
    - echo "Releasing..."
    - echo "Tag = ${CI_COMMIT_TAG}"
    - echo "URL = ${PACKAGE_REGISTRY_URL}/${CI_COMMIT_TAG}"
    - echo "JAVA = ${ASSET}"
    - |
      release-cli --debug create --name "Release ${CI_COMMIT_TAG}" --tag-name "${CI_COMMIT_TAG}" --assets-link "${ASSET}"
    - echo "Done!"

  rules:
    - if: '$CI_COMMIT_TAG && $CI_COMMIT_TAG =~ /^v[0-9]\.[0-9]\.[0-9](-rc.+)?/'
      when: always

build-doc:
  stage: build
  needs:
    - build
  script:
    - echo "version = \"${CI_COMMIT_TAG}\"" | sed -e "s/ = \"v/ = \"/g" >> build.gradle
    - cat build.gradle
    - gradle javadoc
  artifacts:
    paths:
      - build/docs/javadoc
  rules:
    - if: '$CI_COMMIT_TAG && $CI_COMMIT_TAG =~ /^v[0-9]\.[0-9]\.[0-9](-rc.+)?/'
      when: always

pages:
  stage: release
  script:
    - mkdir public
    - cp -r build/docs/javadoc/* public/
  artifacts:
    paths:
      - public/
  needs:
    - job: build-doc
      artifacts: true
  rules:
    - if: '$CI_COMMIT_TAG && $CI_COMMIT_TAG =~ /^v[0-9]\.[0-9]\.[0-9](-rc.+)?/'
      when: always
